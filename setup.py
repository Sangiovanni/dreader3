from setuptools import setup

setup(
    name='DReader2',
    version='1.0',
    packages=[''],
    url='',
    license='MIT',
    author='Daniele',
    author_email='sangiovanni.daniele@gmail.com',
    description='Seven Segment display OCR', install_requires=['cv2']
)
