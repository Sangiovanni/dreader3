import argparse
import os
from devices import Device
from main_config import StartConf
from main_start import StartProg
DEFAULT_CAMERA = 0


def handle(args):
    dev_path = os.path.dirname(os.path.realpath(__file__)) + '/devices/'
    dev = Device()

    if args.opt == 'start':
        #print('start device: '+args.devname+' camera: '+str(args.cam))
        sp = StartProg(args.devname, dev_path + args.devname, args.cam)
        sp.start()

    elif args.opt == 'set':
        # print(dev_path)
        sc = StartConf(args.devname, dev_path + args.devname, args.cam)
        sc.start()
        #print('configure device: '+args.devname+' camera: '+str(args.cam))
        #startconf(args.devname, args.cam)
    else:
        if args.opt == 'create':
            dev.createdev(args.devname)
        elif args.opt == 'delete':
            dev.deletedev(args.devname)
        elif args.opt == 'rename':
            dev.renamedev(args.devname, args.newname)
        dev.showlist()
        print("\nuse -h for help!")


def main():
    global DEFAULT_CAMERA

    parser = argparse.ArgumentParser()

    subparsers = parser.add_subparsers(help='commands available', title='command', dest='opt')

    # list devices
    list_parser = subparsers.add_parser('list', help='Show a list of available devices')

    # A create command
    create_parser = subparsers.add_parser('create', help='Create a new device')
    create_parser.add_argument('devname', action='store', help='New device to create')

    # A delete command
    delete_parser = subparsers.add_parser('delete', help='Remove a device')
    delete_parser.add_argument('devname', action='store', help='The device to remove')

    # A rename command
    rename_parser = subparsers.add_parser('rename', help='Rename a device')
    rename_parser.add_argument('devname', action='store', help='The device to rename')
    rename_parser.add_argument('newname', action='store', help='The new device name')

    # A setting command
    setting_parser = subparsers.add_parser('set', help='Device setting')
    setting_parser.add_argument('devname', action='store', help='The device to open')
    setting_parser.add_argument('--cam', type=int, dest='cam', default = DEFAULT_CAMERA,
                                help='The index of camera to open. -1 is capture from saved fotos, usefull for debug.')

    # A start command
    start_parser = subparsers.add_parser('start', help='Device start reading')
    start_parser.add_argument('devname', action='store', help='The device to open')
    start_parser.add_argument('--cam', type=int, dest='cam',
                              help='camera index. If -1 capture frame from photo. Useful for debugging (default=' + str(
                                  DEFAULT_CAMERA) + ')', default = DEFAULT_CAMERA)
    args = parser.parse_args()
    handle(args)


# print(args)


if __name__ == "__main__":
    # execute only if run as a script
    main()
