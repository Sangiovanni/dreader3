import cv2
import copy
import math

class WinDigit():
	def __init__(self):
		# radiobutton's position
		self.digit_radiobtn_pos = {'d5': (55, 25), 'd4': (100, 24), 'd3': (145, 24), 'd2': (192, 25), 'd1': (241, 24)}
		self.segment_radiobtn_pos = {'a': (146, 75), 'b': (193, 115), 'c': (194, 196), 'd': (144, 238), 'e': (98, 196),
								'f': (98, 113), 'g': (146, 155), 'dp': (230, 234)}
		self.img = cv2.imread('display7.png')
		self.imgcopy = None
		self.selected_segment = 'a'  # default value on start
		self.selected_digit = 'd1'   # default value on start
		cv2.namedWindow("setup")
		cv2.setMouseCallback('setup', self.setSelectedDisplayAndSegment)

	def imgUpdate(self):
		self.imgcopy = copy.copy(self.img)
		# update view radiobutton segment
		for key in self.segment_radiobtn_pos:
			thick = 3 if self.selected_segment == key else 1
			cv2.circle(self.imgcopy, self.segment_radiobtn_pos[key], 14, (25, 100, 80), thick)
		# update view radiobutton digit
		for key in self.digit_radiobtn_pos:
			thick = 3 if self.selected_digit == key else 1
			cv2.circle(self.imgcopy, self.digit_radiobtn_pos[key], 20, (255, 255, 255), thick)

	def viewupdate(self):
		self.imgUpdate()
		cv2.imshow('setup', self.imgcopy)


	# mouse click actions
	def setSelectedDisplayAndSegment(self, event, x, y, flags, param):
		if event == cv2.EVENT_LBUTTONDOWN:
			# print("x : % 3d, y : % 2d" %(x, y))
			self.setSelectedDigit(event, x, y, flags, param)  # act nur if the click is on a related radio button
			self.setSelectedSegment(event, x, y, flags, param)  # act nur if the click is on a related radio button

	def setSelectedSegment(self, event, x, y, flags, param):
		for key in self.segment_radiobtn_pos:
			dist = self.get_point_distance((x, y), self.segment_radiobtn_pos[key])
			if dist < 14:  # radiobutton 's ray
				self.selected_segment = key
				break

	def setSelectedDigit(self, event, x, y, flags, param):
		# if event == cv2.EVENT_LBUTTONDOWN:
		for key in self.digit_radiobtn_pos:
			dist = self.get_point_distance((x, y), self.digit_radiobtn_pos[key])
			if dist < 20:  # radiobutton 's ray
				self.selected_digit = key
				break

	def get_point_distance(self, a, b):
		return math.sqrt(math.pow(a[0] - b[0], 2) + math.pow(a[1] - b[1], 2))


if __name__ == "__main__":
	wd = WinDigit()
	while (1):
		wd.viewupdate()
		inp_char = cv2.waitKey(100)
		if inp_char == 27:  # ESC
			break

