import cv2
import numpy as np

class Draw():

    @staticmethod
    def drawSegment(img, arr_coord):
        for value in arr_coord:
            cv2.circle(img, (value[0], value[1]), 1, (255, 255, 0), -1)
        return img

    @staticmethod
    def drawRect(img, P0, P1):
        P0 = tuple(P0)
        P1 = tuple(P1)
        RECT_COLOR = (0, 255, 255)

        if P0 != (0, 0) and P1 == (0, 0):
            cv2.line(img, P0, (P0[0], P0[1] + 20), RECT_COLOR, 2)
            cv2.line(img, P0, (P0[0] + 20, P0[1]), RECT_COLOR, 2)
        elif P0 != (0, 0) and P1 != (0, 0):

            cv2.rectangle(img, (P0[0], P0[1]), (P1[0], P1[1]), RECT_COLOR, 2)
        return img


if __name__ == "__main__":
    pass
