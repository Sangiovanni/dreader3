import cv2
import os
# ImgCapture, acquires images from webcam or from files.
# Returns an array containing original image, a copy of the original and grayscale image
# PARAMETER:
# cam_num = camera's index
# if cam_num == -1  capture from file
# pathdir Subdirectory of the /images folder from which photos are uploaded or saved.
# The /images folder is created automatically, if it does not exist
# Saved images will have the following name: bild_[n].png

class ImgCapture():
    def __init__(self, cam_num, pathdir):
        self.cam_num = cam_num
        self.pathdir = pathdir
        self.file_index = 1       # Useful in case of capture from file
        if self.cam_num != -1:
            self.camera = cv2.VideoCapture(cam_num, cv2.CAP_DSHOW)
            if not self.camera.isOpened():
                raise ValueError('Cannot open camera ' + str(cam_num))
        else:
            if os.path.exists(self.pathdir + '/images') and self.getNumFilesInDir(self.pathdir + "/images/") > 0:
                self.last_frame = cv2.imread(self.pathdir + "/images/bild_1.png", cv2.IMREAD_ANYCOLOR)
            else:
                self.last_frame = cv2.imread('noimage.png', cv2.IMREAD_ANYCOLOR)

    def saveImage(self, img):
        if not os.path.exists(self.pathdir + '/images'):
            os.makedirs(self.pathdir + '/images')
        dir = self.pathdir + "/images/"
        filename = dir + "bild_" + str(len(os.listdir(dir)) + 1) + ".png"
        cv2.imwrite(filename, img)

    def getNextFile(self):
        if (self.cam_num == -1):
            if os.path.exists(self.pathdir + '/images'):
                num_files = self.getNumFilesInDir(self.pathdir + "/images/")
                if (self.file_index < num_files):
                    self.file_index = self.file_index + 1
                    self.last_frame = cv2.imread(self.pathdir + '/images/bild_' + str(self.file_index) + '.png',
                                                 cv2.IMREAD_ANYCOLOR)

    def getPrevFile(self):
        if (self.cam_num == -1):
            if (self.file_index != 1):
                self.file_index = self.file_index - 1
                self.last_frame = cv2.imread(self.pathdir + '/images/bild_' + str(self.file_index) + '.png',
                                             cv2.IMREAD_ANYCOLOR)

    def getNumFilesInDir(self, dir):
        count = 0
        for path in os.listdir(dir):
            if os.path.isfile(os.path.join(dir, path)):
                count += 1
        return count

    def captureFrame(self):
        if self.cam_num != -1:
            if not self.camera.isOpened():
                raise ValueError('Camera ist not opened!')
            else:
                ret, self.last_frame = self.camera.read()
        img = {}
        img['orig'] = self.last_frame
        img['col'] = self.last_frame.copy()
        img['gray'] = cv2.cvtColor(self.last_frame, cv2.COLOR_BGR2GRAY)
        return img

    def getImgIndex(self):
        return self.file_index


if __name__ == "__main__":
    cam_num = 0
    cv2.namedWindow("color")
    cv2.namedWindow("grayscale")
    ic = ImgCapture(cam_num, "devices/test/")
    while (1):
        img = ic.captureFrame()
        if cam_num == -1:
            cv2.putText(img['col'], 'Bild: ' + str(ic.getImgIndex()), (500, 460), cv2.FONT_HERSHEY_SIMPLEX, 0.6,
                        (0, 255, 255), 1, cv2.LINE_AA, False)
        cv2.imshow("color", img['col'])
        cv2.imshow("grayscale", img['gray'])
        inp_char = cv2.waitKey(100)
        if inp_char == 27:  # ESC
            cv2.destroyAllWindows()
            break
        elif cam_num!=-1 and (inp_char == ord('s') or inp_char == ord('S')):
            ic.saveImage(img['orig'])
        elif cam_num == -1:    #input from saved image
            if inp_char == ord('+'):
                ic.getNextFile()
            elif inp_char == ord('-'):
                ic.getPrevFile()

