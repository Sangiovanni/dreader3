import cv2
from settingfilecoord import Setup
from draw import Draw


class ControlHandle(Setup):
    def __init__(self, pathdir, Display):
        self.pathdir = pathdir
        self.display = Display
        self.mouse_btn_is_clicked = False
        super().__init__(pathdir)



        cv2.createTrackbar("Threshold_bar", "filter", self.getThreshold(), 255, self.setThreshold)

        cv2.setMouseCallback("color", self.setCoord)

    def confReload(self):
        self.loadFromFile()

    def confSave(self):
        self.onFileSave()

    def rectangleDelete(self):
        self.resetRectCoord()

    def segmentDelete(self):
        self.delSegmentCoord(self.display.selected_digit, self.display.selected_segment)

    def setCoord(self, event, x, y, flags, param):

        if event == cv2.EVENT_LBUTTONDOWN:
            self.mouse_btn_is_clicked = True
            if self.getP1() == (0, 0) or self.getP0() == (0, 0):
                self.setRectCoord((x, y))
                self.mouse_btn_is_clicked = False
                return
        if self.mouse_btn_is_clicked:
            self.setSegmentCoord(self.display.selected_digit, self.display.selected_segment, (x, y))
        if event == cv2.EVENT_LBUTTONUP:
            self.mouse_btn_is_clicked = False
            return

    def draw_all_segments(self, image):
        for digit in self.dat['digits']:
            for segment in self.dat['digits'][digit]:
                coord = self.getSegmentCoord(digit, segment)
                Draw.drawSegment(image, coord)
