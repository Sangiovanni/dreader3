import cv2
from analysis import Analysis
import numpy as np

class DataLogger:
    def __init__(self, pathdir):
        cv2.namedWindow("datalogger")
        self.blank_image = np.zeros(shape=[160, 550, 3], dtype=np.uint8)
        self.blank_image[:, :] = [200, 200, 200]
        cv2.putText(self.blank_image, "Reading:", (35, 20), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 0), 1)
        self.blank = self.blank_image.copy()
        self.analysis = Analysis(pathdir)
        self.prev_val = ''
        self.act_val = ''

    def draw_meas_rect(self, textval):
        cv2.rectangle(self.blank, (30, 30), (520, 130), (255, 255, 255), -1)
        cv2.putText(self.blank, textval, (35, 100), cv2.FONT_HERSHEY_SIMPLEX, 2.8, (0, 0, 0), 3)

    def read_update(self, gray_image):
        actual_value = self.analysis.display_read(gray_image)
        self.draw_meas_rect(actual_value)
        cv2.imshow("datalogger", self.blank)

