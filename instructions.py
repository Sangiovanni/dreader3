import cv2
import numpy as np

class Instructions():
    def __init__(self, cam_num):
        cv2.namedWindow("instructions")
        cv2.moveWindow("instructions", 642, 0)
        self.cam_num = cam_num
        self.blank_image = np.ones(shape=[200, 350, 3], dtype=np.uint8)

    def optionsshow(self):
        if self.cam_num != -1:
            self.options.append("I: Save image")
        if self.cam_num == -1:
            self.options.append("+/-: Switch image")
        for i in range(len(self.options)):
            cv2.putText(self.blank_image, self.options[i], (3, 25 * (1 + i)), cv2.FONT_HERSHEY_SIMPLEX, 0.5,
                        (0, 255, 0), 1,
                        cv2.LINE_AA, False)
        cv2.imshow("instructions", self.blank_image)


class InsUse(Instructions):
    def __init__(self, cam_num):
        super().__init__(cam_num)
        self.options = ["ESC: Program quit", "S: Start reading", "P: Pause reading"]
        self.optionsshow()




class InsSetup(Instructions):
    def __init__(self, cam_num):
        super().__init__(cam_num)
        # create window function keys description
        self.options = ["Q: Save and quit", "ESC: Quit without saving", "D: Discard changes",
                        "R: Delete rectangle", "S: Delete display segment",
                        "A: Show all segments (ON/OFF)."]
        self.optionsshow()


if __name__ == "__main__":
    InsSetup(-1)
    while (1):
        inp_char = cv2.waitKey(100)
        if inp_char == 27:  # ESC
            break






