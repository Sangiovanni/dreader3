import os
#device configuration files are saved in a folder that has the same name as the device. All folders with
# configurations are inside the 'devices' folder

class Device():

    def __init__(self):
        self.dev_path = os.path.dirname(os.path.realpath(__file__)) + '/devices/'

    def showlist(self):
        ldir = os.listdir(self.dev_path)
        print('\ndevice list:\n')
        print(*ldir, sep="\n")

    def createdev(self, devname):
        os.mkdir(self.dev_path + devname)

    def deletedev(self, devname):
        os.system('rm -rf ' + self.dev_path + devname)

    def renamedev(self, oldname, newname):
        os.rename(self.dev_path + oldname, self.dev_path + newname)

