from  instructions import InsSetup
from settingdigitshow import WinDigit
from imagecapture import ImgCapture
from controls import ControlHandle
from draw import Draw
import cv2


class StartConf:
    def __init__(self, device_name, path, cam_num):
        cv2.namedWindow("color")
        cv2.namedWindow("filter")
        self.device_name = device_name
        self.path = path
        self.cam_num = cam_num
        InsSetup(self.cam_num)     # open window with instructions
        self.windigit = WinDigit()  # show an window with digit and radiobutton
        self.capture = ImgCapture(cam_num, path)
        self.controls = ControlHandle(path, self.windigit)       # mouse events and image filter
        self.en_show_all_segment = False

    def viewsupdate(self):
        self.windigit.viewupdate()           # update view window with digit and radiobutton
        # imagecapture
        self.img = self.capture.captureFrame()
        if self.cam_num == -1:
            cv2.putText(self.img['col'], 'Bild: ' + str(self.capture.getImgIndex()), (500, 460), cv2.FONT_HERSHEY_SIMPLEX,
                        0.6, (0, 255, 255), 1, cv2.LINE_AA, False)

        # rectangle draw
        Draw.drawRect(self.img['col'], self.controls.getP0(), self.controls.getP1())

        if self.en_show_all_segment:
            self.controls.draw_all_segments(self.img['col'])
        else:
            # single segment draw
            Draw.drawSegment(self.img['col'], self.controls.getSegmentCoord(self.windigit.selected_digit,
                                                                        self.windigit.selected_segment))

        cv2.imshow("color", self.img['col'])
        # filter image
        cv2.threshold(self.img['gray'], self.controls.getThreshold(), 255, cv2.THRESH_BINARY, self.img['gray'])
        cv2.imshow("filter", self.img['gray'])


    def programQuit(self):
        cv2.destroyAllWindows()

    def confSave(self):
        self.controls.onFileSave()

    def confReload(self):
        self.controls.confReload()

    def rectangleDelete(self):
        self.controls.rectangleDelete()

    def segmentDelete(self):
        self.controls.segmentDelete()

    def showall(self):
        self.en_show_all_segment = not self.en_show_all_segment

    def saveImage(self):
        self.capture.saveImage(self.img['orig'])

    def getNextFile(self):
        self.capture.getNextFile()

    def getPrevFile(self):
        self.capture.getPrevFile()

    def start(self):

        while (1):
            self.viewsupdate()
            inp_char = cv2.waitKey(100)
            if inp_char == 27:  # ESC
                self.programQuit()
                break
            elif inp_char == ord('q') or inp_char == ord('Q'):
                self.confSave()
                startconfig.programQuit()
                break
            elif inp_char == ord('a') or inp_char == ord('A').as_integer_ratio():
                self.showall()
            elif inp_char == ord('d') or inp_char == ord('D'):
                self.confReload()
            elif inp_char == ord('r') or inp_char == ord('R'):
                self.rectangleDelete()
            elif inp_char == ord('s') or inp_char == ord('S'):
                self.segmentDelete()
            elif self.cam_num != -1 and (inp_char == ord('i') or inp_char == ord('I')):
                self.saveImage()
            elif self.cam_num == -1:  # input from saved image
                if inp_char == ord('+'):
                    self.getNextFile()
                elif inp_char == ord('-'):
                    self.getPrevFile()



if __name__ == "__main__":
    cam_num = -1
    startconfig = StartConf("test", "devices/test", cam_num)
    startconfig.start()



