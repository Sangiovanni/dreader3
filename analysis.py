from settingfilecoord import Setup


class Analysis(Setup):


    def __init__(self, pathdir):
        super().__init__(pathdir)
        self.map = {
            'hhhhhhhh': '',
            'hhhhhhhd': '.',
            'ddddddhh': '0',
            'ddddddhd': '0.',
            'hddhhhhh': '1',
            'hddhhhhd': '1.',
            'ddhddhdh': '2',
            'ddhddhdd': '2.',
            'ddddhhdh': '3',
            'ddddhhdd': '3.',
            'hddhhddh': '4',
            'hddhhddd': '4.',
            'dhddhddh': '5',
            'dhddhddd': '5.',
            'dhdddddh': '6',
            'dhdddddd': '6.',
            'dddhhhhh': '7',
            'dddhhhhd': '7.',
            'dddddddh': '8',
            'dddddddd': '8.',
            'ddddhddh': '9',
            'ddddhddd': '9.',
        }
        self.segments = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'dp']
        self.digits = ['d5', 'd4', 'd3', 'd2', 'd1']

    # 'd': dark, 'h': hell or unknow
    def get_segment_status(self, gray_image, digit, segment):
        if digit not in self.dat['digits'] or segment not in self.dat['digits'][digit]:  # not mapped
            return 'h'
        cnt_hell_pixels = 0
        cnt_dark_pixels = 0
        for p in self.dat['digits'][digit][segment]:
            if gray_image[p[1], p[0]] < self.getThreshold():
                cnt_dark_pixels += 1
            else:
                cnt_hell_pixels += 1

        if cnt_dark_pixels > cnt_hell_pixels:
            return 'd'
        else:
            return 'h'

    def display_read(self, gray_image):
        issue = ''
        for digit in self.digits:
            keymap = ''
            for segment in self.segments:
                keymap += self.get_segment_status(gray_image, digit, segment)
            if keymap in self.map:
                issue = issue + self.map[keymap]
            else:
                issue += '*'
        return issue
