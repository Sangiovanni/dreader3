import cv2
from instructions import InsUse
from imagecapture import ImgCapture
from datalogger import DataLogger
from settingfilecoord import Setup
from draw import Draw


class StartProg:
    def __init__(self, device_name, path, cam_num):
        self.device_name = device_name
        self.path = path
        self.cam_num = cam_num
        self.setup = Setup(self.path)
        cv2.namedWindow("camera")
        InsUse(self.cam_num)     # open window with instructions
        self.capture = ImgCapture(self.cam_num, path)
        self.img = None
        self.datalogger = DataLogger(self.path)

    def program_quit(self):
        cv2.destroyAllWindows()

    def save_image(self):
        self.capture.saveImage(self.img['orig'])

    def get_next_file(self):
        self.capture.getNextFile()

    def get_prev_file(self):
        self.capture.getPrevFile()

    def viewsupdate(self):
        # image capture
        self.img = self.capture.captureFrame()
        self.datalogger.read_update(self.img['gray'])
        Draw.drawRect(self.img['col'], self.setup.getP0(), self.setup.getP1())
        if self.cam_num == -1:
            cv2.putText(self.img['col'], 'Bild: ' + str(self.capture.getImgIndex()), (500, 460), cv2.FONT_HERSHEY_SIMPLEX,
                        0.6, (0, 255, 255), 1, cv2.LINE_AA, False)
        cv2.imshow("camera", self.img['col'])

    def start(self):
        while (1):
            self.viewsupdate()
            inp_char = cv2.waitKey(100)
            if inp_char == 27:  # ESC
                self.program_quit()
                break
            elif self.cam_num != -1 and (inp_char == ord('i') or inp_char == ord('I')):
                self.save_image()
            elif self.cam_num == -1:  # input from saved image
                if inp_char == ord('+'):
                    self.get_next_file()
                elif inp_char == ord('-'):
                    self.get_prev_file()


if __name__ == "__main__":
    cam_num = 0
    startprog = StartProg("test", "devices/test", cam_num)
    startprog.start()